import java.util.*;
import java.io.*;

public class GameHelper{

	private static final String alphabet = "abcdefg";
	private int gridLength = 7;
	private int gridSize = 49;
	private int [] grid = new int[gridSize];
	private int comCount = 0;

	public String getUserInput(String prompt){
		String inputLine = null;
		System.out.print(prompt);
		try {
			BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
			inputLine = is.readLine();
		}
		catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		
		if (inputLine.length() > 2 || inputLine.length() == 0){
			inputLine = "invalid";
		}
		else {
			String coordX = String.valueOf(inputLine.charAt(0));
			int coordY = Integer.parseInt(String.valueOf(inputLine.charAt(1)));
			boolean matchLetter = false;
			boolean matchNumber = false;
			for (int x = 0; x < gridLength; x++){
				if (x == coordY){
					matchNumber = true;
				}
				if (String.valueOf(alphabet.charAt(x)).equals(coordX)){
					matchLetter = true;
				}
			}
			if (!matchLetter || !matchNumber){
				inputLine = "invalid";
			}
		}

		return inputLine;
	}


	//method to set the dot com 'battleship' in a 7x7 grid.
	public ArrayList<String> placeDotCom(int comSize){
		ArrayList<String> alphaCells = new ArrayList<String>();
		
		String temp = null;
		int [] coords = new int[comSize];
		int attempts = 0;
		boolean success = false;
		int location = 0;

		comCount++;
		int incr = 1;
		if ((comCount % 2) == 1){
			incr = gridLength;
		}
		
		//try and place the dotCom in the grid without overlapping any other DotComs.
		while ( !success & attempts++ <200){
			location = (int) (Math.random() * gridSize);
			int x = 0;
				success = true;
				while (success && x < comSize){
					if (grid[location] == 0) {
						coords[x++] = location;
						location += incr;
						if (location >= gridSize){
							success = false;
						}
						if (x>0 && (location % gridLength == 0)){
							success = false;
						}
					}
					else {
						success = false;
					}
				}
		}
		
		int x = 0;
		int row = 0;
		int column = 0;
		while (x < comSize){
			grid[coords[x]] = 1;
			row = (int) (coords[x] / gridLength);
			column = coords[x] % gridLength;
			temp = String.valueOf(alphabet.charAt(column));

			alphaCells.add(temp.concat(Integer.toString(row)));
			x++;
		}
		
		return alphaCells;
	}

}
