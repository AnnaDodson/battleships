import java.util.*;

//Test class to test the game by running through every square on the grid
public class DotComTestDrive{

	private GameHelper helper = new GameHelper();
	private Grid grid = new Grid();
	private ArrayList<DotCom> dotComList = new ArrayList<DotCom>();
	private int numOfGuesses = 0;

	private String alphabet = "abcdefg";

	public static void main (String[] args){
		DotComTestDrive game = new DotComTestDrive();
		game.startGame();
	}

	private void startGame(){
		//Make the DotComs, give them a location and add them into the array list.
		DotCom one = new DotCom();
		one.setName("Pets.com");
		DotCom two = new DotCom();
		two.setName("eToys.com");
		DotCom three = new DotCom();
		three.setName("Go2.com");
		
		dotComList.add(one);
		dotComList.add(two);
		dotComList.add(three);

		//For each dot com in the array, get a location and send it to setter
		for (DotCom dotComToSet : dotComList){
			ArrayList<String> newLocation = helper.placeDotCom(3);
			dotComToSet.setLocationCells(newLocation);
		}

		grid.createGrid();
		grid.printGrid();

		//Test to run through every option in the grid to check
			for (int x = 0; x < 7; x++){
				if (dotComList.isEmpty()){
					break;
				}
				String letter = String.valueOf(alphabet.charAt(x));
				for (int y = 0; y < 7; y++){
						String number = Integer.toString(y);
						String guess = letter + number;
						System.out.println(guess);

						String result = checkUserGuess(guess);
						System.out.println(result);

						grid.setGrid(guess, result);
						grid.printGrid();
					if (dotComList.isEmpty()){
						break;
					}
				}
			}

		System.out.println("All the Dot Coms are dead");
	}

	private String checkUserGuess(String userGuess){
		String result = "miss";

		for (DotCom dotComToTest : dotComList){
			result = dotComToTest.checkYourself(userGuess);
			if (result.equals("hit")) {
				break;
			}
			if (result.equals("kill")) {
				dotComList.remove(dotComToTest);
				break;
			}
		}
		return result;
	}

}
