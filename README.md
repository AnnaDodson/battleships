# Battleships!

Small command line game to learn java. Instead of ships, sink the DotComs by giving coordinates on a 7x7 grid.

Example Game:

    Enter a guess: a3
		miss
    Enter a guess: a4
		miss
    Enter a guess: a5
		hit
    Enter a guess: b5
		hit
    Enter a guess: c5
		Ouch! You sunk Pets.com
		kill

From Head First Java (http://www.headfirstlabs.com/books/hfjava/)

I've updated the game to now include a grid view:

    a|X|O|X|X|_|X|_
    b|_|O|_|X|_|_|X
    c|_|O|X|O|O|_|_
    d|X|_|_|X|_|X|X
    e|_|_|X|X|_|X|_
    f|_|X|_|_|X|_|_
    g|_|_|O|O|O|_|X
      0 1 2 3 4 5 6

