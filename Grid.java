public class Grid{

	private String [][] grid = new String[8][8];
	private String alphabet = "abcdefg ";

	//create the empty grid.
	public void createGrid (){
		for (int x = 0; x < 8; x++){
			for (int y = 0; y < 8; y++){
				grid[x][y] = "|_";
				if (x == 7) {
					grid[x][y] = " " + (y-1);
				}
				if (y == 0){
					grid[x][y] = String.valueOf(alphabet.charAt(x));
				}
			}
		}
	}


	public void setGrid (String coord, String result){
		String square = null;
		String x = String.valueOf(coord.charAt(0));
		int y = Integer.parseInt(String.valueOf(coord.charAt(1)));
		if (result.equals("miss")){
			square = "|X";
		}
		else if (result.equals("hit") || result.equals("kill")){
			square = "|O";
		}
		for (int i = 0; i < 8; i++){
			if (grid[i][0].equals(x)){
				grid[i][y+1] = square;
			}
		}
	}

	public void printGrid (){
		for (String [] line : grid){
			for (String cell : line){
				System.out.print(cell);
			}
		System.out.print("\n");
		}
	}

}
